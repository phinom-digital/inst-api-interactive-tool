# Phinom digital institutional api interactive tool

## How to

```
docker build -t inst-apit-interactive-tool . 
docker run -d -p 8080:8080 -e API_PORT=8080 -e "API_URL=https://ins.test.phinom-digital.dev" -e API_UPLOAD_TIME=30 -t inst-apit-interactive-tool
```

Open browser on localhost:8080