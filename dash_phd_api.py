import dash_trich_components as dtc
from dash import Dash, dcc, html, Input, Output, State, MATCH, ALL
import dash_bootstrap_components as dbc
import dash
from dash import Input, Output, dcc, html,ctx
from dash.dependencies import Input, Output, State
import pandas as pd
from dash import dash_table
import time
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import threading
import test_phdapi_class as phdapi
import ui_elements as ui
import os
import dash_loading_spinners as dls

TEST = False
API_PORT = os.environ.get('API_PORT',default=8080)
API_UPLOAD_TIME = os.environ.get('API_UPLOAD_TIME',default=40)
def upload_instruments(data):
    while (True):
        tmp = ui.UiProcess(key='',secret='',load_instuments=True,test=TEST)
        data = tmp.instruments
        time.sleep(60)

ui_process = ui.UiProcess('','',load_instuments=False,test=TEST)
ui_first = ui.UiProcess(key='',secret='',load_instuments=True,test=TEST)

instruments_id_data = ui_first.instruments

x = threading.Thread(target=upload_instruments, args=(instruments_id_data,), daemon=True)
x.start()

first_flag= True
refresh_time = int(time.time())

#LAYOUT_SETTINGS
spin_out = html.Div(id="loading-output")
spinner = dbc.Row(dbc.Col(dls.Beat(spin_out,width=50,color='#3d7d72')))
layout_setting = html.Div(children=[ui.img_row,spinner,ui.auth_row,],id='layout_sett')
#LAYOUT_API
layout_api = html.Div([ ui.img_row,
                        html.Div(dbc.Row([dbc.Col(ui.header,width=12,style = {'margin-top':'0px'})])),
                        html.Div(ui.collapse_func,style = {'padding-top':'35px'}),
                        html.Div(ui.collapse_func_orders,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_position,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_getportfolios,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_postportfolios,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_posportfolio,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_price,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_index,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_balance,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_depaddr,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_getwthdr,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_putwthdr,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_putordr,style = {'padding-top':'5px'}),
                        html.Div(ui.collapse_func_delordr,style = {'padding-top':'5px','padding-bottom':'120px'}),
                        dcc.Interval(
                            id='interval-component',
                            interval=10*1000,
                            n_intervals=0
                        )
                        ],
                      style = {'overflowX':'hidden'},
                      id = 'layout_api'
                      )

content_api = html.Div(layout_api,
                       id = 'content_api',
                       style={'margin-left':'0px','margin-bottom':'50px',
                              'margin-right':'50px',"overflowX": "hide",})
content_auth = html.Div(children=[layout_setting],id='content_auth')
layout = html.Div([ dtc.SideBar([ dtc.SideBarItem(id='id_3', label="API", icon="far fa-list-alt"),
                                  dtc.SideBarItem(id='id_5', label="Settings", icon="fa-solid fa-lock")]),
                    dcc.Store(id='secret_value', storage_type='session'),
                    dcc.Store(id='key_value', storage_type='session'),
                    html.Div([ ],
                             style={'width':'90%',
                                    "overflowX": "hide",
                                    "overflowY": "hide"},
                             id="page_content")])


sidebar = dbc.Container(dtc.SideBar([ dtc.SideBarItem(id='id_3', label="API", icon="far fa-list-alt"),
                                 dtc.SideBarItem(id='id_5', label="Settings", icon="fa-solid fa-lock")]),
                        fluid=True,
                        )

layout = html.Div( [dbc.Row([ dbc.Col(sidebar,width=1),
                             dbc.Col(html.Div([ ],
                                              style={'width':'100%',
                                                     "overflowX": "hide",
                                                     "overflowY": "hide"},
                                              id="page_content"),
                                     width=11,
                                     )

                            ],
                            style={'margin-left':'20px','overflowX':'hide'}
                            ),
                    dcc.Store(id='secret_value', storage_type='session'),
                    dcc.Store(id='key_value', storage_type='session'),
                    ],
                   id = 'layout',
                   style = {'overflow':'hidden'}
)
#APP SERVER
app = dash.Dash(external_stylesheets=[dbc.icons.FONT_AWESOME])
server = app.server
app.layout = layout
app._favicon = './favicon.png'
app.title = 'InstitutionalApi'


#CALLBACKS
@app.callback(
    Output("page_content", "children"),
    Input("id_3", "n_clicks_timestamp"),
    Input("id_5", "n_clicks_timestamp"),
    State('key_value','data'),
    State('secret_value','data'),
)
def toggle_collapse(input3,input5,key,secret):
    btn_df = pd.DataFrame({"input3": [input3],
                           "input5": [input5]})
    btn_df = btn_df.fillna(0)
    if btn_df.idxmax(axis=1).values == "input3":
        return content_api,
    if btn_df.idxmax(axis=1).values == "input5":
        ui_process = ui.UiProcess(key,secret,False,TEST)
        input_key = dbc.InputGroup([ dbc.InputGroupText("APIkey"),
                                     dbc.Input(placeholder="Input apikey..",
                                               type = 'text',
                                               value = ui_process.phd_api.api_key,
                                               id='key_input')])
        input_secret = dbc.InputGroup([ dbc.InputGroupText("Secret"),
                                        dbc.Input(placeholder="Input secret..",
                                                  type = 'text',
                                                  value = ui_process.phd_api.secret,
                                                  id='secret_input')])
        auth_input = html.Div(children=[html.Div(input_key,style={'margin-top':'25px'}),
                                        html.Div(input_secret,style={'margin-top':'25px'})],)
        auth_card = dbc.Card(dbc.CardBody([ html.H5("Autorization", className="card-title"),
                                            auth_input,
                                            dbc.Button("save",
                                                       id = 'save',
                                                       style={'background-color':'#3d7d72',
                                                              'border-color':'#91d8df',
                                                              'margin-top':'25px','align':'right'})]))
        auth_row = dbc.Row(dbc.Col(auth_card,
                                   width={'size':6,'offset':3},),
                           style={'text-align':'center','margin-top':'150px'})
        layout_setting = html.Div([ ui.img_row,spinner, auth_row,],style={'height':'100vh'})
        content_auth = html.Div([layout_setting])
        return content_auth,

#AUTORIZATION
@app.callback(
    Output("save", "n_clicks"),
    Output("key_input", "value"),
    Output("secret_input", "value"),
    Output('key_value','data'),
    Output('secret_value','data'),
    Output("loading-output",'children'),
    Input("key_input", "value"),
    Input("secret_input", "value"),
    Input("save", "n_clicks"),
    State('key_value','data'),
    State('secret_value','data'),
)
def autorize(key,secret,n,key_value,secret_value):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id == 'save':
        ui_process = ui.UiProcess(key_value,secret_value,False,TEST)
        if n >= 1:
            if (key is not None) or (secret is not None):
                new_key = ui_process.phd_api.api_key
                new_secret = ui_process.phd_api.secret
                if key is not None:
                    new_key = key
                    key_value = new_key
                if secret is not None:
                    new_secret = secret
                    secret_value = new_secret
                ui_process = ui.UiProcess(new_key,new_secret,False,TEST)
                time.sleep(2)
                n = 0
                return n,key,secret,key_value,secret_value,''
    raise dash.exceptions.PreventUpdate("cancel the callback")
#INSTUMENTS
@app.callback(
    Output("instuments_button", "n_clicks"),
    Output('instuments_output', "children"),
    Output('instruments_symbol', "value"),
    Output('instruments_optype', "value"),
    Input("instuments_button", "n_clicks"),
    Input('instuments_output', "children"),
    Input('instruments_symbol', "value"),
    Input('instruments_optype', "value"),
    Input('instruments_prodtype', "value"),
    Input('instruments_active', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def instruments_callback(n,output,symbol,optype,type_p,is_active,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != "instuments_button":
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == "instuments_button"):
        ui_process = ui.UiProcess(key,secret,load_instuments=True,test=TEST)
        active = None
        try:
            active = ui.CONSTANTS[is_active]
        except:
            pass
        df = ui_process.phd_api.instruments(symbol,option_type=optype,type_p=type_p,is_active=active)
        if (df is None) or (df.shape[0]==0):
            output = dbc.Alert(ui_process.phd_api.client.err, color="danger")
            ui_process.phd_api.client.err = None
        else:
            df.isExpired = df.isExpired.apply(lambda x: 'True' if x == True else 'False' )
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto"})
            output = table
        n = 0
        return n,output,symbol,optype
    raise dash.exceptions.PreventUpdate("cancel the callback")
#ORDERS
@app.callback(
    Output('orders_button', "n_clicks"),
    Output('output_orders', "children"),
    Input('orders_button', "n_clicks"),
    Input('output_orders', "children"),
    Input('productid_input_orders', "value"),
    Input('start_input_orders', "value"),
    Input('end_input_orders', "value"),
    Input('limit_input_orders', "value"),
    Input('orders_active', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def orders_callback(n,output,productid,start,end,limit,active,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != 'orders_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'orders_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        id_numb = None
        try:
            id_numb = instruments_id_data[instruments_id_data.symbolId == productid]['instrumentId'].values.tolist()[0]
        except:
            pass
        df = ui_process.phd_api.orders(product_id=id_numb,is_active=active,start_time=start,end_time=end,limit=limit)
        if (df is None) or (df.shape[0]==0):
            try:
                if df.shape[0]==0:
                    if ui_process.phd_api.client.err is None:
                        ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
        else:

            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto"})
            output = table
        n = 0
        return n,output,
    raise dash.exceptions.PreventUpdate("cancel the callback")

#PRICE
@app.callback(
    Output('price_button', "n_clicks"),
    Output('output_price', "children"),
    Input('price_button', "n_clicks"),
    Input('output_price', "children"),
    Input('productid_input_price', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def price_callback(n,output,productid,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != 'price_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'price_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        id_numb = None
        try:
            id_numb = instruments_id_data[instruments_id_data.symbolId == productid]['instrumentId'].values.tolist()[0]
        except:
            pass
        df = ui_process.phd_api.price(product_id=id_numb)
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    if ui_process.phd_api.client.err is None:
                        ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#POSITION
@app.callback(
    Output('position_button', "n_clicks"),
    Output('output_position', "children"),
    Input('position_button', "n_clicks"),
    Input('output_position', "children"),
    Input('productid_input_position', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def positions_callback(n,output,productid,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != 'position_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'position_button' ):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        id_numb = None
        try:
            id_numb = instruments_id_data[instruments_id_data.symbolId == productid]['instrumentId'].values.tolist()[0]
        except:
            pass
        df = ui_process.phd_api.positions(product_id=id_numb)
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#BALANCE
@app.callback(
    Output('balance_button', "n_clicks"),
    Output('output_balance', "children"),
    Input('balance_button', "n_clicks"),
    Input('output_balance', "children"),
    State('key_value','data'),
    State('secret_value','data'),
)
def balance_callback(n,output,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'balance_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'balance_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.balance()
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#GET WITHDRAW
@app.callback(
    Output('getwthdr_button', "n_clicks"),
    Output('output_getwthdr', "children"),
    Input('getwthdr_button', "n_clicks"),
    Input('output_getwthdr', "children"),
    State('key_value','data'),
    State('secret_value','data'),
)
def getwthdr_callback(n,output,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'getwthdr_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n==1) and (triggered_id == 'getwthdr_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.get_withdraw()
        try:
            if (df.shape[0]==1) and (len(df.columns)==0):
                ui_process.phd_api.client.err = 'Empty result'
                output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
                ui_process.phd_api.client.err = None
                n = 0
                return n,output_new
        except:
            pass
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#GET PORTFOLIO
@app.callback(
    Output('getportfolios_button', "n_clicks"),
    Output('output_getportfolios', "children"),
    Input('getportfolios_button', "n_clicks"),
    Input('output_getportfolios', "children"),
    State('key_value','data'),
    State('secret_value','data'),
)
def getportfolios_callback(n,output,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'getportfolios_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n==1) and (triggered_id == 'getportfolios_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.portfolios()
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#DEPOSIT ADDR
@app.callback(
    Output('depaddr_button', "n_clicks"),
    Output('output_depaddr', "children"),
    Input('depaddr_button', "n_clicks"),
    Input('output_depaddr', "children"),
    State('key_value','data'),
    State('secret_value','data'),
)
def deposit_callback(n,output,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'depaddr_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n==1) and (triggered_id == 'depaddr_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.deposit_addr()
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#INDEX
@app.callback(
    Output('index_button', "n_clicks"),
    Output('output_index', "children"),
    Input('index_button', "n_clicks"),
    Input('output_index', "children"),
    Input('index_symbol', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def index_callback(n,output,symbol,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'index_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'index_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.index(symbol)
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#POSITIONS BY PORTFOLIO
@app.callback(
    Output('posportfolio_button', "n_clicks"),
    Output('output_posportfolio', "children"),
    Input('posportfolio_button', "n_clicks"),
    Input('output_posportfolio', "children"),
    Input('productid_input_posportfolio', "value"),
    Input('portfolioid_input_posportfolio', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def posportfolio_callback(n,output,productid,portfolioid,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != 'posportfolio_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'posportfolio_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        id_numb = None
        try:
            id_numb = instruments_id_data[instruments_id_data.symbolId == productid]['instrumentId'].values.tolist()[0]
        except:
            pass
        df = ui_process.phd_api.positions_by_portfolio(id_numb,portfolioid)
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#POST PORTFOLIO
@app.callback(
    Output('postportfolios_button', "n_clicks"),
    Output('output_postportfolios', "children"),
    Input('postportfolios_button', "n_clicks"),
    Input('output_postportfolios', "children"),
    Input('name_input_postportfolios', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def postportfolio_order(n,output,name,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'postportfolios_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n >= 1) and (triggered_id == 'postportfolios_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.post_portfolio(name=name)
        if ui_process.phd_api.client.err is None:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output = table
            n = 0
            return n,output
        else:
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")
#DELETE
@app.callback(
    Output('delordr_button', "n_clicks"),
    Output('output_delordr', "children"),
    Input('delordr_button', "n_clicks"),
    Input('output_delordr', "children"),
    Input('orderid_input_delordr', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def delete_order(n,output,id,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'delordr_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n == 1) and (triggered_id == 'delordr_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.delete_order(order_id=id)
        if ui_process.phd_api.client.err is None:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output = table
            n = 0
            return n,output
        else:
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#POST
@app.callback(
    Output('putordr_button', "n_clicks"),
    Output('output_putordr', "children"),

    Input('putordr_button', "n_clicks"),
    Input('output_putordr', "children"),
    Input('productid_input_putordr', "value"),
    Input('quant_input_putordr', "value"),
    Input('price_input_putordr', "value"),
    Input('putordr_side', "value"),
    Input('portfolioid_input_putordr', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def post_order(n,output,id,quant,price,side,portfolio_id,key,secret):
    global instruments_id_data
    triggered_id = ctx.triggered_id
    if triggered_id != 'putordr_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n >= 1) and (triggered_id == 'putordr_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        id_numb = None
        try:
            id_numb = instruments_id_data[instruments_id_data.symbolId == id]['instrumentId'].values.tolist()[0]
        except:
            pass
        df = ui_process.phd_api.post_order(product_id=id_numb,quantity=quant,price=price,side=side,portfolio_id=portfolio_id)
        if ui_process.phd_api.client.err is None:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output = table
            n = 0
            return n,output
        else:
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")

#POST WITHDRAW
@app.callback(
    Output('putwthdr_button', "n_clicks"),
    Output('output_putwthdr', "children"),
    Input('putwthdr_button', "n_clicks"),
    Input('output_putwthdr', "children"),
    Input('currencyid_input_putwthdr', "value"),
    Input('amount_input_putwthdr', "value"),
    Input('dest_input_putwthdr', "value"),
    State('key_value','data'),
    State('secret_value','data'),
)
def postwthdr_callback(n,output,symbol,amount,dest,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != 'putwthdr_button':
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n>=1) and (triggered_id == 'putwthdr_button'):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = None
        df = ui_process.phd_api.post_withdraw(currency_id=symbol,amount=amount,destination_addr=dest)
        if (df is None) or (df.shape[0] == 0):
            try:
                if df.shape[0]==0:
                    ui_process.phd_api.client.err = 'Empty result'
            except:
                pass
            output_new = dbc.Alert(ui_process.phd_api.client.err, color="danger"),
            ui_process.phd_api.client.err = None
            n = 0
            return n,output_new
        else:
            table = html.Div(dbc.Table.from_dataframe(df,
                                                      striped=True,
                                                      bordered=True,
                                                      hover=True,),
                             style={"maxHeight": "400px", "overflow": "auto",})
            output_new = table
        n = 0
        return n,output_new
    raise dash.exceptions.PreventUpdate("cancel the callback")


#REFRESH INSTRUMENTS OPTIONS
@app.callback(
    Output('productid_input_putordr','options'),
    Output('productid_input_price','options'),
    Output('productid_input_position','options'),
    Output('productid_input_orders','options'),
    Output('productid_input_posportfolio','options'),
    Output('interval-component','n_intervals'),
    Input('productid_input_putordr','options'),
    Input('productid_input_price','options'),
    Input('productid_input_position','options'),
    Input('productid_input_orders','options'),
    Input('productid_input_posportfolio','options'),
    State('key_value','data'),
    State('secret_value','data'),
    Input('interval-component','n_intervals')

)
def refresh_options(options1,options2,options3,options4,options5,key,secret,interval):
    global instruments_id_data,refresh_time,first_flag
    triggered_id = ctx.triggered_id
    curr_time = int(time.time())
    if (triggered_id == 'interval-component'):
        print('interval',interval)
    if (triggered_id == 'interval-component') and ((interval>60*int(API_UPLOAD_TIME)) or (interval == 1)):
        options_arr = []
        refresh_time = curr_time
        ui_process = ui.UiProcess(key,secret,load_instuments=True,test=TEST)
        if ui_process.instruments.shape[0]>0:
            first_flag = False
            options_arr = ui_process.instruments['symbolId'].unique()
        else:
            try:
                options_arr = instruments_id_data['symbolId'].unique()
            except:
                pass
        options1 = options_arr
        options2 = options_arr
        options3 = options_arr
        options4 = options_arr
        options5 = options_arr
        interval = 2
        return options1,options2,options3,options4,options5,interval
    raise dash.exceptions.PreventUpdate("cancel the callback")


#COLLAPSE
@app.callback(
    Output("collapse-instruments", "is_open"),
    [Input("collapse-button-instruments", "n_clicks")],
    [State("collapse-instruments", "is_open")],
)
def toggle_collapse_instruments(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-position", "is_open"),
    [Input("collapse-button-position", "n_clicks")],
    [State("collapse-position", "is_open")],
)
def toggle_collapse_position(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-price", "is_open"),
    [Input("collapse-button-price", "n_clicks")],
    [State("collapse-price", "is_open")],
)
def toggle_collapse_price(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-orders", "is_open"),
    Output("collapse-button-orders", "n_clicks"),
    Input("collapse-button-orders", "n_clicks"),
    Input('productid_input_orders','options'),
    State("collapse-orders", "is_open"),
    State('key_value','data'),
    State('secret_value','data'),
)
def toggle_collapse_orders(n, productid,is_open,key,secret):
    triggered_id = ctx.triggered_id
    if (n >= 1) and (triggered_id == "collapse-button-orders"):
        flag = not is_open
        n = 0
        return flag,n
    raise dash.exceptions.PreventUpdate("cancel the callback")

@app.callback(
    Output("collapse-putordr", "is_open"),
    Output("collapse-button-putordr", "n_clicks"),
    Output('portfolioid_input_putordr', "options"),
    Input("collapse-button-putordr", "n_clicks"),
    Input('portfolioid_input_putordr', "options"),
    State("collapse-putordr", "is_open"),
    State('key_value','data'),
    State('secret_value','data'),
)
def toggle_collapse_postordr(n,portfolio_id, is_open,key,secret):
    triggered_id = ctx.triggered_id
    if (n >= 1) and (triggered_id == "collapse-button-putordr"):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.portfolios()
        try:
            portfolio_id = df['portfolioId'].values
        except:
            pass
        flag_open = not is_open
        n = 0
        return flag_open,n,portfolio_id
    raise dash.exceptions.PreventUpdate("cancel the callback")


@app.callback(
    Output("collapse-delordr", "is_open"),
    [Input("collapse-button-delordr", "n_clicks")],
    [State("collapse-delordr", "is_open")],
)
def toggle_collapse_delordr(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-index", "is_open"),
    [Input("collapse-button-index", "n_clicks")],
    [State("collapse-index", "is_open")],
)
def toggle_collapse_index(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-balance", "is_open"),
    [Input("collapse-button-balance", "n_clicks")],
    [State("collapse-balance", "is_open")],
)
def toggle_collapse_balance(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-depaddr", "is_open"),
    [Input("collapse-button-depaddr", "n_clicks")],
    [State("collapse-depaddr", "is_open")],
)
def toggle_collapse_depaddr(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-putwthdr", "is_open"),
    [Input("collapse-button-putwthdr", "n_clicks")],
    [State("collapse-putwthdr", "is_open")],
)
def toggle_collapse_putwthdr(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-getwthdr", "is_open"),
    [Input("collapse-button-getwthdr", "n_clicks")],
    [State("collapse-getwthdr", "is_open")],
)
def toggle_collapse_getwthdr(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-getportfolios", "is_open"),
    [Input("collapse-button-getportfolios", "n_clicks")],
    [State("collapse-getportfolios", "is_open")],
)
def toggle_collapse_getportfolios(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output("collapse-posportfolio", "is_open"),
    Output("collapse-button-posportfolio", "n_clicks"),
    Output('portfolioid_input_posportfolio', "options"),
    Input("collapse-button-posportfolio", "n_clicks"),
    Input('portfolioid_input_posportfolio', "options"),
    State("collapse-posportfolio", "is_open"),
    State('key_value','data'),
    State('secret_value','data'),
)
def toggle_collapse_posportfolio(n,portfolio_id, is_open,key,secret):
    triggered_id = ctx.triggered_id
    if triggered_id != "collapse-button-posportfolio":
        raise dash.exceptions.PreventUpdate("cancel the callback")
    if (n >= 1) and (triggered_id == "collapse-button-posportfolio"):
        ui_process = ui.UiProcess(key,secret,load_instuments=False,test=TEST)
        df = ui_process.phd_api.portfolios()
        try:
            portfolio_id = df['portfolioId'].values
        except:
            pass
        flag_open = not is_open
        n = 0
        return flag_open,n,portfolio_id
    raise dash.exceptions.PreventUpdate("cancel the callback")

@app.callback(
    Output("collapse-postportfolios", "is_open"),
    [Input("collapse-button-postportfolios", "n_clicks")],
    [State("collapse-postportfolios", "is_open")],
)
def toggle_collapse_postportfolio(n, is_open):
    if n:
        return not is_open
    return is_open

if __name__ == '__main__':
    app.run_server(debug=False,port=API_PORT,host='0.0.0.0')