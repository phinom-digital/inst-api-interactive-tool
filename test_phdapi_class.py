import pandas as pd
import urllib.parse
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import hashlib
import hmac
import json
import base64
import requests
import time
from requests import Session, Request
class InstrumentsRequest:
    def endpoint(self):
        return '/instruments'
class PriceRequest:
    def endpoint(self):
        return '/price'
class PositionsRequest:
    def endpoint(self):
        return '/positions'
class OrdersRequest:
    def endpoint(self):
        return '/orders'
class OrderRequest:
    def endpoint(self):
        return '/order'
class DepositAddrRequest:
    def endpoint(self):
        return '/deposit-addresses'
class BalanceRequest:
    def endpoint(self):
        return '/balance'
class IndexRequest:
    def endpoint(self):
        return '/index'
class TimeRequest:
    def endpoint(self):
        return '/time'
class PortfoliosRequest:
    def endpoint(self):
        return '/portfolios'
class PortfolioRequest:
    def endpoint(self):
        return '/portfolio'
class PosByPortfolioRequest:
    def __init__(self,portfoplioid):
        if portfoplioid is None:
            self.portfolioid = ''
        else:
            self.portfolioid = '/' + portfoplioid
    def endpoint(self,):
        return '/positions'+ self.portfolioid
class WithdrawRequest:
    def endpoint(self):
        return '/withdraw'
class ClientReqClass:
    def __init__(self,url='',
                 key = '',
                 secret = '',
                 test = False):
        self.headers = {
            'api-key': key,
            'api-sign': '',
            'nounce': '',
            'Content-Type': 'application/json',
            'accept': '*/*',
            'Accept-Language': 'en-US,en;q=0.9',

        }
        self.url = url
        self.secret = secret
        self.err = None
        self.test = test
        self.host = url.replace('https://','')

    def update_headers(self,req_method='',payload=None,path=''):
        nonce = int(time.time())
        sign = ''
        try:
            if self.test == True:
                sign = self.get_kraken_signature(nonce, self.secret)
            else:
                sign = self.get_signature(req_method,path,payload,self.secret,nonce)
        except:
            print('Exception update headers')
            pass
        self.headers['nounce'] = str(nonce)
        self.headers['api-sign'] = sign
        pass

    def get(self,method,params):
        print('method',method)
        p = requests.Request('GET', self.url+method, params=params).prepare()
        req_url = p.url
        self.update_headers(req_method='GET',path=method,payload=None)
        response = requests.request("GET", self.url+method, headers=self.headers, params = params,verify=False )
        if response.ok == False:
            self.err = 'Error:'+response.text
            print(self.err)
            return None
        else:
            pass
        y = json.loads(response.text)
        return y
    def post(self,method,payload,payload_raw=''):
        postdata = urllib.parse.urlencode(payload_raw)
        p = requests.Request('POST', self.url+method, params={},data=payload).prepare()
        req_url = p.url
        self.update_headers(req_method='POST',path=method,payload=postdata)
        response = requests.post(url=self.url+method, headers=self.headers,data=payload,verify=False )
        if response.ok == False:
            self.err = 'Error:'+response.text
            print(self.err)
            return None
        y = json.loads(response.text)
        return y
    def delete(self,method,params):
        p = requests.Request('DELETE', self.url+method, params=params).prepare()
        req_url = p.url
        self.update_headers(req_method='DELETE',path=method,payload=None)
        response = requests.request("DELETE", self.url+method, headers=self.headers, params = params,verify=False )
        if response.ok == False:
            self.err = 'Error:'+response.text
            print(self.err)
            return None
        y = json.loads(response.text)
        return y
    def get_kraken_signature(self,nonce, secret):
        mac = hmac.new(secret.encode(), (str(nonce)).encode(), hashlib.sha256)
        sigdigest = base64.b64encode(mac.digest())
        return sigdigest.decode()
    def get_signature(self,req_method,path,payload,secret,nonce):
        sign_data = ''
        if payload is None:
            sign_data = req_method + self.host + path + str(nonce)
        else:
            sign_data = req_method + self.host + path + str(nonce) + payload
            pass

        mac = hmac.new(secret.encode(), sign_data.encode(), hashlib.sha256)
        sigdigest = base64.b64encode(mac.digest())
        sign =  sigdigest.decode()
        return sign


class PhDAPI:
    def __init__(self,api_key,secret,url,test=False):
        self.api_key = api_key
        self.secret = secret
        self.url = url
        self.client = ClientReqClass(self.url,self.api_key,self.secret,test)
        pass

    def instruments(self,symbol,option_type,type_p,is_active):
        req = InstrumentsRequest()
        params = {'assetId':symbol,
                  'type':type_p,
                  'includeExpired':is_active,
                  'optionType':option_type}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame(res)
        return df
    def instruments_id(self,symbol,option_type,type_p,is_active):
        req = InstrumentsRequest()
        params = {'assetId':symbol,
                  'type':type_p,
                  'includeExpired':is_active,
                  'optionType':option_type}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        return res
    def post_order(self,product_id,quantity,price,side,portfolio_id):
        req = OrderRequest()
        data = {
            'instrumentId': product_id,
            'quantity': quantity,
            'price': price,
            'side': side,
            'portfolioId':portfolio_id,
        }
        res = self.client.post(req.endpoint(),payload=json.dumps(data))
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def delete_order(self,order_id):
        req = OrderRequest()
        params={'orderId':order_id}
        res = self.client.delete(req.endpoint(),params=params)
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def orders(self,product_id,is_active,start_time,end_time,limit):
        req = OrdersRequest()
        params = {  'InstrumentId':product_id,
                    'IsActive':is_active,
                    'StartTime':start_time,
                    'EndTime':end_time,
                    'Limit':limit,}
        res = self.client.get(req.endpoint(),params=params)
        df = pd.DataFrame(res)
        return df
    def price(self,product_id):
        req = PriceRequest()
        params = {'InstrumentId':product_id}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def positions(self,product_id):
        req = PositionsRequest()
        params = {'instrumentId':product_id}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame(res)
        return df
    def balance(self,):
        req = BalanceRequest()
        params = {}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        assets = []
        amounts = []
        locked = []
        for elem in res['items']:
            assets.append(elem['asset'])
            amounts.append(elem['amount'])
            locked.append(elem['locked'])
        df = pd.DataFrame()
        df['asset'] = assets
        df['amount'] = amounts
        df['locked'] = locked
        return df
    def time_(self,):
        req = TimeRequest()
        params = {}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def deposit_addr(self,):
        req = DepositAddrRequest()
        params = {}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame()
        df['assetAddress'] = []
        df['asset'] = []
        addr_list = []
        asset_list = []
        for elem in res:
            addr_list.append(elem['assetAddress'])
            asset_list.append(elem['asset'])
        df['assetAddress'] = addr_list
        df['asset'] = asset_list
        return df
    def get_withdraw(self,):
        req = WithdrawRequest()
        params = {}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def post_withdraw(self,currency_id,amount,destination_addr):
        req = WithdrawRequest()
        data = {
            'asset': currency_id,
            'amount': amount,
            'destinationAddress': destination_addr,
        }
        res = self.client.post(req.endpoint(),payload=json.dumps(data))
        if res is None:
            return None
        df = pd.DataFrame()
        df['id'] = [res['id']]
        try:
            df['asset'] = [res['asset']]
        except:
            pass
        try:
            df['amount'] = [res['amount']]
        except:
            pass
        try:
            df['fee'] = [res['fee']]
        except:
            pass
        try:
            df['destinationAddress'] = [res['destinationAddress']]
        except:
            pass
        try:
            df['createTime'] = [res['createTime']]
        except:
            pass
        try:
            df['executionTime'] = [res['executionTime']]
        except:
            pass
        try:
            df['cancelTime'] = [res['cancelTime']]
        except:
            pass
        try:
            df['statusId'] = [res['status']['id']]
        except:
            pass
        try:
            df['statusName'] = [res['status']['name']]
        except:
            pass
        try:
            df['statusClass'] = [res['status']['class']]
        except:
            pass
        return df
    def index(self,symbol):
        req = IndexRequest()
        params = {'asset':symbol}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame()
        df['symbol'] = [res['symbol']]
        df['price'] = [res['price']]
        df['priceTime'] = [res['priceTime']]
        return df
    def post_portfolio(self,name):
        req = PortfolioRequest()
        data = {
            'displayName':name,
        }
        res = self.client.post(req.endpoint(),payload=json.dumps(data))
        if res is None:
            return None
        df = pd.DataFrame([res])
        return df
    def portfolios(self,):
        req = PortfoliosRequest()
        params = {}
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        displayName_list = []
        parentPortfolioId_list = []
        portfolioId_list = []
        for elem in res:
            keys = elem.keys()
            if 'displayName' in keys:
                displayName_list.append(elem['displayName'])
            else:
                displayName_list.append(None)
            if 'parentPortfolioId' in keys:
                parentPortfolioId_list.append(elem['parentPortfolioId'])
            else:
                parentPortfolioId_list.append(None)
            if 'portfolioId' in keys:
                portfolioId_list.append(elem['portfolioId'])
            else:
                portfolioId_list.append(None)
        df = pd.DataFrame()
        df['displayName'] = displayName_list
        df['parentPortfolioId'] = parentPortfolioId_list
        df['portfolioId'] = portfolioId_list
        return df
    def positions_by_portfolio(self,product_id,portfolio_id):
        req = PosByPortfolioRequest(portfolio_id)
        params = {'InstrumentId':product_id,
                  'portfolioId':portfolio_id,
                  }
        res = self.client.get(req.endpoint(),params = params)
        if res is None:
            return None
        df = pd.DataFrame(res)
        return df
