import dash_trich_components as dtc
from dash import Dash, dcc, html, Input, Output, State, MATCH, ALL
import dash_bootstrap_components as dbc
import dash
from dash import Input, Output, dcc, html,ctx
from dash.dependencies import Input, Output, State
import pandas as pd
from dash import dash_table

import test_phdapi_class as phdapi
import os
import dash_loading_spinners as dls


API_URL = os.environ.get('API_URL',default='https://ins.test.phinom-digital.dev')

CONSTANTS = {'True':True,'False':False}

class UiProcess:
    def __init__(self,key,secret,load_instuments=True,test=False):
        self.phd_api = phdapi.PhDAPI(api_key=key,
                              secret = secret,
                              url = API_URL,
                              test=test)
        self.instruments = pd.DataFrame()
        self.instruments['instrumentId'] = []
        self.instruments['symbolId'] = []
        if load_instuments == True:
            try:
                df_list = self.load_instruments_id()
                self.instruments = pd.concat(df_list, ignore_index=True)[['instrumentId','symbolId']]
                self.instruments.drop_duplicates(inplace=True)

            except:
                pass

    def load_instruments_id(self):
        res_list = []
        for symbol in [None]:
                for flag in [False]:
                    tmp = self.phd_api.instruments(symbol=symbol,option_type='',is_active=flag,type_p='')
                    tmp.drop_duplicates(inplace=True)
                    res_list.append(tmp)
        return res_list

def create_instruments_row(postfix):
    label_symbol = html.Div(html.Label('Symbol:',style={'background-color':'rgb(237, 242, 242)'}))
    input_symbol = dcc.Dropdown( options=['ETH','BTC'],
                                        id = 'symbol_input_'+postfix)
    param_symbol = html.Div([label_symbol,input_symbol])

    label_date = html.Div(html.Label('Expiration:',style={'background-color':'rgb(237, 242, 242)'}))
    input_date = dcc.Dropdown( options=[],
                                id = 'date_input_'+postfix)
    param_date = html.Div([label_date,input_date])

    label_type = html.Div(html.Label('ProductType:',style={'background-color':'rgb(237, 242, 242)'}))
    input_type = dcc.Dropdown( options=['Binary','Touch','Warrant'],
                               id = 'type_input_'+postfix)
    param_type = html.Div([label_type,input_type])

    label_side = html.Div(html.Label('Type:',style={'background-color':'rgb(237, 242, 242)'}))
    input_side = dcc.Dropdown( options=['Call','Put'],
                               id = 'side_input_'+postfix)
    param_side = html.Div([label_side,input_side])

    label_strike = html.Div(html.Label('Strike:',style={'background-color':'rgb(237, 242, 242)'}))
    input_strike = dcc.Dropdown( options=[],
                               id = 'strike_input_'+postfix)
    param_strike = html.Div([label_strike,input_strike])

    instruments_params_row = dbc.Row([ dbc.Col(param_symbol,width=2),
                                       dbc.Col(param_date,width=2),
                                       dbc.Col(param_type,width=2),
                                       dbc.Col(param_strike,width=2),
                                       dbc.Col(param_side,width=2)])
    return instruments_params_row


#HEADERS
header = html.Div(html.H2('InstitutionalApi'),style={'text-align':'center',})
img_obj = html.Div(html.Img(src = '/assets/logo.png',height=60),style={'text-align':'center',})
img_row = dbc.Row([dbc.Col([img_obj],style = {'padding-top':'20px'},
                           width={"size": 12, "offset": 0}),],id = 'logo',
                  style={ 'width':'100%',"overflowX": "hide","overflowY": "hide"},)

#AUTORIZATION
input_key = dbc.InputGroup(
    [
        dbc.InputGroupText("APIkey"),
        dbc.Input(placeholder="Input apikey..",
                  type = 'text',
                  id='key_input'),
    ],

),
input_secret = dbc.InputGroup([ dbc.InputGroupText("Secret"),
                                dbc.Input(placeholder="Input secret..",
                                          type = 'text',
                                          id='secret_input'),],)
auth_input = html.Div(children=[html.Div(input_key,style={'margin-top':'25px'}),
                                html.Div(input_secret,style={'margin-top':'25px'})],)
auth_card = dbc.Card(dbc.CardBody([ html.H5("Autorization", className="card-title"),
                                    auth_input,
                                    dbc.Button("save",
                                               id = 'save',
                                               style={'background-color':'#3d7d72','margin-top':'25px',
                                                      'border-color':'#91d8df',
                                                      'align':'right'})]))
auth_row = dbc.Row(dbc.Col(auth_card,
                           width={'size':6,'offset':3},),
                   style={'text-align':'center','margin-top':'150px'})
spinner = dbc.Row(dbc.Col(dls.Wave(width=80)))
layout_setting = html.Div([img_row,spinner,auth_row,])

#INSTRUMENTS
collapse_output = html.Div([ dbc.Button("Output",
                                        id="collapse-button",
                                        size = 'sm',
                                        color="primary",
                                        n_clicks=0,),
                             dbc.Collapse(dbc.Card(dbc.CardBody("This content is hidden in the collapse")),
                                          id="collapse",
                                          is_open=False,)],
                           style = {'padding-top':'20px'})

label_symbol = html.Label('Symbol:',style={'background-color':'rgb(237, 242, 242)'})
dropdown_symbol = dcc.Dropdown(options=['BTC', 'ETH'],
                               id = 'instruments_symbol')
param_symbol = html.Div([label_symbol,dropdown_symbol])

label_type = html.Label('Option Type:',style={'background-color':'rgb(237, 242, 242)'})
dropdown_type = dcc.Dropdown(options=['CALL', 'PUT'],
                             id = 'instruments_optype')

label_prodtype = html.Label('ProductType:',style={'background-color':'rgb(237, 242, 242)'})
dropdown_prodtype = dcc.Dropdown(options=[ 'Binary','Touch','Warrant'],
                               id = 'instruments_prodtype')

label_active_inst = html.Label('Include Expired:',style={'background-color':'rgb(237, 242, 242)'})
dropdown_active_inst = dcc.Dropdown(options=['True', 'False'],
                             id = 'instruments_active')

param_symbol = html.Div([label_symbol,dropdown_symbol],style={'display':'inline'})
param_optype = html.Div([label_type,dropdown_type],style={'display':'inline'})
param_prodtype = html.Div([label_prodtype,dropdown_prodtype],style={'display':'inline'})
param_active_inst = html.Div([label_active_inst,dropdown_active_inst],style={'display':'inline'})

instruments_params_row = dbc.Row([ dbc.Col(param_symbol,width=2),
                                   dbc.Col(param_prodtype,width=4),
                                   dbc.Col(param_active_inst,width=4),
                                   dbc.Col(param_optype,width=2)]),

inst_button = html.Div([html.Label('Method:',style={'background-color':'rgb(237, 242, 242)'}),
                        dbc.Button('Instruments',size='sm',
                                   id = 'instuments_button',
                                   style={'vertical-align':'center','display':'block',
                                          'background-color':'#3d7d72','border-color':'#91d8df',})])
function_row = dbc.Row([ dbc.Col([inst_button],width=2),
                         dbc.Col(instruments_params_row,width=10)],
                       style = {'padding-top':'10px'})

#func_output = dbc.Row([ dbc.Col(children=[],width={'size':12,'offset':0},id='instuments_output')],
                      #style = {'padding-top':'10px'})
func_output = dbc.Row([ dbc.Col(dls.Beat(html.Div(children=[],id='instuments_output'),width=30,color='#3d7d72'),
                                width={'size':12,'offset':0},)],
                        style = {'padding-top':'10px'})
row_label = dbc.Row(dbc.Col(html.Label('Output:',style={'background-color':'rgb(237, 242, 242)'}),width=1),
                    style = {'padding-top':'10px'})
collapse_func = html.Div([ html.Button("Get list of Instruments",
                                       id="collapse-button-instruments",
                                       n_clicks=0,
                                       style = {'align':'center','width':'100%'}),
                           dbc.Collapse([ function_row,row_label,func_output],
                                        id="collapse-instruments",
                                        is_open=False,)],
                         style = {'padding-top':'10px'})

#PUT_ORDER
label_productid_putordr = html.Div(html.Label('Instrument:',style={'background-color':'rgb(237, 242, 242)'}))
input_prdid_putordr = dcc.Dropdown(options=[],
                                   id = 'productid_input_putordr')
param_prdid_putordr = html.Div([label_productid_putordr,input_prdid_putordr])

label_quant_putordr = html.Div(html.Label('Quantity:',style={'background-color':'rgb(237, 242, 242)'}))
input_quant_putordr = html.Div(dbc.Input(placeholder="",id='quant_input_putordr',type='number'),)
param_quant_putordr = html.Div([label_quant_putordr,input_quant_putordr])

label_price_putordr = html.Div(html.Label('Price:',style={'background-color':'rgb(237, 242, 242)'}))
input_price_putordr = html.Div(dbc.Input(placeholder="Input price..",id='price_input_putordr',type='number'),)
param_price_putordr = html.Div([label_price_putordr,input_price_putordr])

label_portfolioid_putordr = html.Div(html.Label('Portfolio Id:',style={'background-color':'rgb(237, 242, 242)'}))
input_portfolioid_putordr = html.Div(   dcc.Dropdown(options=[],
                                        id='portfolioid_input_putordr',))
param_portfolioid_putordr = html.Div([label_portfolioid_putordr,input_portfolioid_putordr])

label_side_putordr = html.Div(html.Label('Side:',style={'background-color':'rgb(237, 242, 242)'}))
side_drpdwn_putordr = dcc.Dropdown(options=['BUY', 'SELL'],id = 'putordr_side',placeholder='')
param_side_putordr = html.Div([label_side_putordr,side_drpdwn_putordr],style={'display':'inline'})

putordr_params_row = dbc.Row([ dbc.Col(param_prdid_putordr,width=3),
                               dbc.Col(param_quant_putordr,width=2),
                               dbc.Col(param_portfolioid_putordr,width=4),
                               dbc.Col(param_price_putordr,width=2),
                               dbc.Col(param_side_putordr,width=1)])

putordr_button = html.Div([ html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Post',
                                       size='sm',
                                       id = 'putordr_button',
                                       style={'vertical-align':'center','display':'block',
                                              'background-color':'#3d7d72','border-color':'#91d8df',})])
function_row_putordr = dbc.Row([ dbc.Col([putordr_button],width=1),
                                 dbc.Col(putordr_params_row,width=11)],
                               style = {'padding-top':'10px'})
func_output_putordr = dbc.Row([ dbc.Col([],width=12,id='output_putordr')],style = {'padding-top':'10px'})
collapse_func_putordr = html.Div([ html.Button("Post order",
                                               id="collapse-button-putordr",
                                               n_clicks=0,
                                               style = {'align':'center','width':'100%'},),
                                   dbc.Collapse([ function_row_putordr,
                                                  row_label,
                                                  func_output_putordr],
                                                id="collapse-putordr",
                                                is_open=False,)],
                                 style = {'padding-top':'20px'})

#DELETE ORDER
label_orderid_delordr = html.Div(html.Label('Order Id:',style={'background-color':'rgb(237, 242, 242)'}))
input_orderid_delordr = html.Div(dbc.Input(placeholder="Input order id..",id='orderid_input_delordr',type='text'),)
param_orderid_delordr = html.Div([label_orderid_delordr,input_orderid_delordr])

delordr_params_row = dbc.Row([ dbc.Col(param_orderid_delordr,width=5)]),

delordr_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                           dbc.Button('Delete',size='sm',
                                      id = 'delordr_button',
                                      style={'background-color':'#3d7d72',
                                             'border-color':'#91d8df',
                                             'display':'block'})])
function_row_delordr = dbc.Row([ dbc.Col([delordr_button],width=1),
                                 dbc.Col(delordr_params_row,width=11)],
                               style = {'padding-top':'10px'})
func_output_delordr = dbc.Row([ dbc.Col(children=[],width=12,id='output_delordr')],
                              style = {'padding-top':'10px'})
collapse_func_delordr = html.Div([ html.Button("Delete order",
                                               id="collapse-button-delordr",
                                               n_clicks=0,
                                               style = {'align':'center','width':'100%'}),
                                   dbc.Collapse([ function_row_delordr,
                                                  row_label,
                                                  func_output_delordr],
                                                id="collapse-delordr",
                                                is_open=False,)],
                                 style = {'padding-top':'20px'},)
#PRICE
label_productid_price = html.Div(html.Label('Instrument:',style={'background-color':'rgb(237, 242, 242)'}))
input_prdid_price = dcc.Dropdown(options=[],
                                 id = 'productid_input_price')
param_prdid_price = html.Div([label_productid_price,input_prdid_price])

price_params_row = dbc.Row([ dbc.Col(param_prdid_price,width=5)])
price_button = html.Div([ html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                          dbc.Button('Price',
                                     size='sm',
                                     id = 'price_button',
                                     style={'background-color':'#3d7d72',
                                            'border-color':'#91d8df',
                                            'display':'block'})])
function_row_price = dbc.Row([ dbc.Col([price_button],width=1),
                               dbc.Col(price_params_row,width=11)],
                             style = {'padding-top':'10px'})

func_output_price = dbc.Row([ dbc.Col(children=[],width=12,id='output_price')],
                            style = {'padding-top':'10px'})
collapse_func_price = html.Div([ html.Button("Price",
                                             id="collapse-button-price",
                                             n_clicks=0,
                                             style = {'align':'center','width':'100%'}),
                                 dbc.Collapse([ function_row_price,
                                                row_label,
                                                func_output_price],
                                              id="collapse-price",
                                              is_open=False)],
                               style = {'padding-top':'20px'})
#POSITIONS
label_productid_position = html.Div(html.Label('Instrument:',style={'background-color':'rgb(237, 242, 242)'}))
input_prdid_position = dcc.Dropdown(options=[],
                                    id = 'productid_input_position')
param_prdid_position = html.Div([label_productid_position,input_prdid_position])

position_params_row = dbc.Row([ dbc.Col(param_prdid_position,width=5)])
position_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Positions',
                                       size='sm',
                                       id = 'position_button',
                                       style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_position = dbc.Row([ dbc.Col([position_button],width=1),
                                  dbc.Col(position_params_row,width=11)],
                                style = {'padding-top':'10px'})

func_output_position = dbc.Row([ dbc.Col([],width=12,id='output_position')],
                               style = {'padding-top':'10px'})
collapse_func_position = html.Div([ html.Button("Positions",
                                                id="collapse-button-position",
                                                n_clicks=0,
                                                style = {'align':'center','width':'100%'}),
                                    dbc.Collapse([ function_row_position,
                                                   row_label,
                                                   func_output_position],
                                                 id="collapse-position",
                                                 is_open=False)],
                                  style = {'padding-top':'20px'})

#ORDERS
label_productid_orders = html.Div(html.Label('Instrument:',style={'background-color':'rgb(237, 242, 242)'}))
input_prdid_orders = dcc.Dropdown(options=[],
                                  id = 'productid_input_orders')
param_prdid_orders = html.Div([label_productid_orders,input_prdid_orders])

label_start_orders = html.Div(html.Label('Start time:',style={'background-color':'rgb(237, 242, 242)'}))
input_start_orders = html.Div(dbc.Input(placeholder="YYYY-MM-DD",id='start_input_orders'),)
param_start_orders = html.Div([label_start_orders,input_start_orders])

label_end_orders = html.Div(html.Label('End time:',style={'background-color':'rgb(237, 242, 242)'}))
input_end_orders = html.Div(dbc.Input(placeholder="YYYY-MM-DD",id='end_input_orders'),)
param_end_orders = html.Div([label_end_orders,input_end_orders])

label_limit_orders = html.Div(html.Label('Limit:',style={'background-color':'rgb(237, 242, 242)'}))
input_limit_orders = html.Div(dbc.Input(placeholder="Input limit..",id='limit_input_orders'),)
param_limit_orders = html.Div([label_limit_orders,input_limit_orders])

label_active_orders = html.Div(html.Label('IsActive:',style={'background-color':'rgb(237, 242, 242)'}))
ative_drpdwn_orders = dcc.Dropdown(options=['True', 'False'],
                                   id = 'orders_active')
param_active_orders = html.Div([label_active_orders,ative_drpdwn_orders],style={'display':'inline'})
orders_params_row = dbc.Row([ dbc.Col(param_prdid_orders,width=4),
                              dbc.Col(param_start_orders,width=2),
                              dbc.Col(param_end_orders,width=2),
                              dbc.Col(param_limit_orders,width=2),
                              dbc.Col(param_active_orders,width=2)]),

orders_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                          dbc.Button('Orders',size='sm',
                                     id = 'orders_button',
                                     style={'vertical-align':'center','display':'block',
                                            'border-color':'#91d8df',
                                            'background-color':'#3d7d72'})])
function_row_orders = dbc.Row([ dbc.Col([orders_button],width=1),
                                dbc.Col(orders_params_row,width=11)],
                              style = {'padding-top':'10px'})
func_output_orders = dbc.Row([ dbc.Col([],width=12,id='output_orders')],
                             style = {'padding-top':'20px'})
collapse_func_orders = html.Div([ html.Button("Get orders list",
                                              id="collapse-button-orders",
                                              n_clicks=0,
                                              style = {'align':'center','width':'100%'}),
                                  dbc.Collapse([ function_row_orders,
                                                 row_label,
                                                 func_output_orders],
                                               id="collapse-orders",
                                               is_open=False)],
                                style = {'padding-top':'20px'})


#INDEX
label_symbol_indx = html.Label('Symbol:',style={'background-color':'rgb(237, 242, 242)'})
dropdown_symbol_indx = dcc.Dropdown(options=['BTC', 'ETH'],
                               id = 'index_symbol')
param_symbol_indx = html.Div([label_symbol_indx,dropdown_symbol_indx])

index_params_row = dbc.Row([ dbc.Col(param_symbol_indx,width=5)])
index_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Index',
                                       size='sm',
                                       id = 'index_button',
                                       style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_index = dbc.Row([ dbc.Col([index_button],width=1),
                                  dbc.Col(index_params_row,width=11)],
                                style = {'padding-top':'10px'})

func_output_index = dbc.Row([ dbc.Col([],width=12,id='output_index')],
                               style = {'padding-top':'10px'})
collapse_func_index = html.Div([ html.Button("Index",
                                                id="collapse-button-index",
                                                n_clicks=0,
                                                style = {'align':'center','width':'100%'}),
                                    dbc.Collapse([ function_row_index,
                                                   row_label,
                                                   func_output_index],
                                                 id="collapse-index",
                                                 is_open=False)],
                                  style = {'padding-top':'20px'})

#BALANCE
balance_params_row = []
balance_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                         dbc.Button('Balance',
                                    size='sm',
                                    id = 'balance_button',
                                    style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_balance = dbc.Row([ dbc.Col([balance_button],width=1),
                               dbc.Col(balance_params_row,width=11)],
                             style = {'padding-top':'10px'})

func_output_balance = dbc.Row([ dbc.Col([],width=12,id='output_balance')],
                            style = {'padding-top':'10px'})
collapse_func_balance = html.Div([ html.Button("Balance",
                                             id="collapse-button-balance",
                                             n_clicks=0,
                                             style = {'align':'center','width':'100%'}),
                                 dbc.Collapse([ function_row_balance,
                                                row_label,
                                                func_output_balance],
                                              id="collapse-balance",
                                              is_open=False)],
                               style = {'padding-top':'20px'})

#DEPOSIT ADDRESSES
depaddr_params_row = []
depaddr_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                           dbc.Button('Addresses',
                                      size='sm',
                                      id = 'depaddr_button',
                                      style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_depaddr = dbc.Row([ dbc.Col([depaddr_button],width=1),
                                 dbc.Col(depaddr_params_row,width=11)],
                               style = {'padding-top':'10px'})

func_output_depaddr = dbc.Row([ dbc.Col([],width=12,id='output_depaddr')],
                              style = {'padding-top':'10px'})
collapse_func_depaddr = html.Div([ html.Button("Deposit addresses",
                                               id="collapse-button-depaddr",
                                               n_clicks=0,
                                               style = {'align':'center','width':'100%'}),
                                   dbc.Collapse([ function_row_depaddr,
                                                  row_label,
                                                  func_output_depaddr],
                                                id="collapse-depaddr",
                                                is_open=False)],
                                 style = {'padding-top':'20px'})

#POST WITHDRAW
label_currencyid_putwthdr = html.Div(html.Label('Symbol:',style={'background-color':'rgb(237, 242, 242)'}))
input_currencyid_putwthdr = dcc.Dropdown(options=['BTC','ETH'],
                                   id = 'currencyid_input_putwthdr')
param_currencyid_putwthdr = html.Div([label_currencyid_putwthdr,input_currencyid_putwthdr])

label_amount_putwthdr = html.Div(html.Label('Amount:',style={'background-color':'rgb(237, 242, 242)'}))
input_amount_putwthdr = html.Div(dbc.Input(placeholder="Input amount..",id='amount_input_putwthdr',type='number'),)
param_amount_putwthdr = html.Div([label_amount_putwthdr,input_amount_putwthdr])

label_dest_putwthdr = html.Div(html.Label('Destination:',style={'background-color':'rgb(237, 242, 242)'}))
input_dest_putwthdr = html.Div(dbc.Input(placeholder="Input destination address..",id='dest_input_putwthdr',type='string'),)
param_dest_putwthdr = html.Div([label_dest_putwthdr,input_dest_putwthdr])
putwthdr_params_row = dbc.Row([ dbc.Col(param_currencyid_putwthdr,width=3),
                               dbc.Col(param_amount_putwthdr,width=3),
                               dbc.Col(param_dest_putwthdr,width=3)])

putwthdr_button = html.Div([ html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Withdraw',
                                       size='sm',
                                       id = 'putwthdr_button',
                                       style={'vertical-align':'center','display':'block',
                                              'background-color':'#3d7d72','border-color':'#91d8df',})])
function_row_putwthdr = dbc.Row([ dbc.Col([putwthdr_button],width=1),
                                 dbc.Col(putwthdr_params_row,width=11)],
                               style = {'padding-top':'10px'})
func_output_putwthdr = dbc.Row([ dbc.Col([],width=12,id='output_putwthdr')],style = {'padding-top':'10px'})
collapse_func_putwthdr = html.Div([ html.Button("Post withdraw operation",
                                               id="collapse-button-putwthdr",
                                               n_clicks=0,
                                               style = {'align':'center','width':'100%'},),
                                   dbc.Collapse([ function_row_putwthdr,
                                                  row_label,
                                                  func_output_putwthdr],
                                                id="collapse-putwthdr",
                                                is_open=False,)],
                                 style = {'padding-top':'20px'})

#GET WITHDRAW
getwthdr_params_row = []
getwthdr_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                           dbc.Button('Withdraw',
                                      size='sm',
                                      id = 'getwthdr_button',
                                      style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_getwthdr = dbc.Row([ dbc.Col([getwthdr_button],width=1),
                                 dbc.Col(getwthdr_params_row,width=11)],
                               style = {'padding-top':'10px'})

func_output_getwthdr = dbc.Row([ dbc.Col([],width=12,id='output_getwthdr')],
                              style = {'padding-top':'10px'})
collapse_func_getwthdr = html.Div([ html.Button("Get withdraw operation",
                                               id="collapse-button-getwthdr",
                                               n_clicks=0,
                                               style = {'align':'center','width':'100%'}),
                                   dbc.Collapse([ function_row_getwthdr,
                                                  row_label,
                                                  func_output_getwthdr],
                                                id="collapse-getwthdr",
                                                is_open=False)],
                                 style = {'padding-top':'20px'})

#GET PORTFOLIOS
getportfolios_params_row = []
getportfolios_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Portfolios',
                                       size='sm',
                                       id = 'getportfolios_button',
                                       style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_getportfolios = dbc.Row([ dbc.Col([getportfolios_button],width=1),
                                  dbc.Col(getportfolios_params_row,width=11)],
                                style = {'padding-top':'10px'})

func_output_getportfolios = dbc.Row([ dbc.Col([],width=12,id='output_getportfolios')],
                               style = {'padding-top':'10px'})
collapse_func_getportfolios = html.Div([ html.Button("Get portfolios",
                                                id="collapse-button-getportfolios",
                                                n_clicks=0,
                                                style = {'align':'center','width':'100%'}),
                                    dbc.Collapse([ function_row_getportfolios,
                                                   row_label,
                                                   func_output_getportfolios],
                                                 id="collapse-getportfolios",
                                                 is_open=False)],
                                  style = {'padding-top':'20px'})

#POST PORTFOLIO

label_name_postportfolios = html.Div(html.Label('Name:',style={'background-color':'rgb(237, 242, 242)'}))
input_name_postportfolios = html.Div(dbc.Input(placeholder="Input name..",id='name_input_postportfolios'),)
param_name_postportfolios = html.Div([label_name_postportfolios,input_name_postportfolios])

postportfolios_params_row = dbc.Row([ dbc.Col(param_name_postportfolios,width=5)])
postportfolios_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                                 dbc.Button('Post',
                                            size='sm',
                                            id = 'postportfolios_button',
                                            style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_postportfolios = dbc.Row([ dbc.Col([postportfolios_button],width=1),
                                       dbc.Col(postportfolios_params_row,width=11)],
                                     style = {'padding-top':'10px'})

func_output_postportfolios = dbc.Row([ dbc.Col([],width=12,id='output_postportfolios')],
                                    style = {'padding-top':'10px'})
collapse_func_postportfolios = html.Div([ html.Button("Post portfolios",
                                                     id="collapse-button-postportfolios",
                                                     n_clicks=0,
                                                     style = {'align':'center','width':'100%'}),
                                         dbc.Collapse([ function_row_postportfolios,
                                                        row_label,
                                                        func_output_postportfolios],
                                                      id="collapse-postportfolios",
                                                      is_open=False)],
                                       style = {'padding-top':'20px'})

#GET POSITIONS BY PORTFOLIO
label_productid_posportfolio = html.Div(html.Label('Instrument:',style={'background-color':'rgb(237, 242, 242)'}))
input_prdid_posportfolio = dcc.Dropdown(options=[],
                                    id = 'productid_input_posportfolio')
param_prdid_posportfolio = html.Div([label_productid_posportfolio,input_prdid_posportfolio])
label_portfolioid_posportfolio = html.Div(html.Label('Portfolio Id:',style={'background-color':'rgb(237, 242, 242)'}))
input_portfolioid_posportfolio = dcc.Dropdown(options=[],
                                        id = 'portfolioid_input_posportfolio')
param_portfolioid_posportfolio = html.Div([label_portfolioid_posportfolio,input_portfolioid_posportfolio])

posportfolio_params_row = dbc.Row([ dbc.Col(param_prdid_posportfolio,width=4),
                                    dbc.Col(param_portfolioid_posportfolio,width=5),
                                  ])

posportfolio_button = html.Div([html.Label('Method:',style={'display':'block','background-color':'rgb(237, 242, 242)'}),
                            dbc.Button('Positions',
                                       size='sm',
                                       id = 'posportfolio_button',
                                       style={'background-color':'#3d7d72','display':'block','border-color':'#91d8df',})])
function_row_posportfolio = dbc.Row([ dbc.Col([posportfolio_button],width=1),
                                  dbc.Col(posportfolio_params_row,width=11)],
                                style = {'padding-top':'10px'})

func_output_posportfolio = dbc.Row([ dbc.Col([],width=12,id='output_posportfolio')],
                               style = {'padding-top':'10px'})
collapse_func_posportfolio = html.Div([ html.Button("Positions by portfolio",
                                                id="collapse-button-posportfolio",
                                                n_clicks=0,
                                                style = {'align':'center','width':'100%'}),
                                    dbc.Collapse([ function_row_posportfolio,
                                                   row_label,
                                                   func_output_posportfolio],
                                                 id="collapse-posportfolio",
                                                 is_open=False)],
                                  style = {'padding-top':'20px'})